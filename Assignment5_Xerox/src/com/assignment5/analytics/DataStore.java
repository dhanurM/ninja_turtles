/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;
import java.util.HashMap;
import java.util.Map;
import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
/**
 *
 * @author dhanur10
 */
public class DataStore {
      private static DataStore dataStore;
    
    private Map<Integer, Customer> customer; 
    private Map<Integer, Item> items; 
    private Map<Integer, Order> orders; 
    private Map<Integer, Product> products; 
    private Map<Integer, SalesPerson> salesPersons; 
    
    private DataStore()
    {
        customer = new HashMap<>();
        items = new HashMap<>();
        orders = new HashMap<>();
        products = new HashMap<>();
        salesPersons = new HashMap<>();
    }

    public Map<Integer, Customer> getCustomer() {
        return customer;
    }

    public void setCustomer(Map<Integer, Customer> customer) {
        this.customer = customer;
    }

    public Map<Integer, Item> getItems() {
        return items;
    }

    public void setItems(Map<Integer, Item> items) {
        this.items = items;
    }

    public Map<Integer, Order> getOrders() {
        return orders;
    }

    public void setOrders(Map<Integer, Order> orders) {
        this.orders = orders;
    }

    public Map<Integer, Product> getProducts() {
        return products;
    }

    public void setProducts(Map<Integer, Product> products) {
        this.products = products;
    }

    public Map<Integer, SalesPerson> getSalesPersons() {
        return salesPersons;
    }

    public void setSalesPersons(Map<Integer, SalesPerson> salesPersons) {
        this.salesPersons = salesPersons;
    }
    
    public static DataStore getInstance(){
    if(dataStore == null)
        dataStore = new DataStore();
    return dataStore;
    }
    
    public static DataStore getDataStore() {
        return dataStore;
    }

    public static void setDataStore(DataStore dataStore) {
        DataStore.dataStore = dataStore;
    }    
    
    
}
