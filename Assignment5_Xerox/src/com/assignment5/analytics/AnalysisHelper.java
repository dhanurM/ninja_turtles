/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhanur10
 */
public class AnalysisHelper {
    
   public static List<Integer> revenueList;
    private Map<Integer, Float> prod_avg = new HashMap<>();
     public void top3BestNegotiatedProducts() {
       Map<Integer, Integer> productssales = new HashMap<Integer, Integer>();
       Map<Integer, Item> item = DataStore.getInstance().getItems();
       Map<Integer, Product> products = DataStore.getInstance().getProducts();
        
        for (Item i : item.values()) {
             if(i.getSalesPrice() > products.get(i.getProductId()).getTargetPrice()) {
                 if (productssales.containsKey(i.getProductId()))
                {
                  int quantity = productssales.get(i.getProductId());
                productssales.put(i.getProductId(), (quantity + i.getQuantity()));
                }
                else {
                productssales.put(i.getProductId(), i.getQuantity());
            }

            } 
        }
         List<Map.Entry<Integer, Integer>> entrylist = new ArrayList<Map.Entry<Integer, Integer>>(productssales.entrySet());
        
        Collections.sort(entrylist, new Comparator<Map.Entry<Integer, Integer>>(){
            @Override
            public int compare(Map.Entry<Integer, Integer> p1,
                               Map.Entry<Integer, Integer> p2)
            {     
                return (p2.getValue()).compareTo(p1.getValue());
            }
           
             
        });
        
       //Print top 3 products
        System.out.println("\n \n Top 3 Negotiated Products according to the quantity are as below:");
        for(int j = 0;j<3;j++){
            System.out.println("\n Product: " +entrylist.get(j)+" ");
        }

      }
     
     public void threeBestCustomer() {
       LinkedHashMap<Integer, Integer> threeCustomers = new LinkedHashMap<Integer, Integer>();
       Map<Integer, Order> order = DataStore.getInstance().getOrders();
       Map<Integer, Product> product = DataStore.getInstance().getProducts();
       
       for(Order o:order.values()){
           if(threeCustomers.containsKey(o.getCustomerId())){
               int sumOfDifference = threeCustomers.get(o.getCustomerId());
               threeCustomers.put(o.getCustomerId(),sumOfDifference +Math.abs((o.getItem().getSalesPrice() - product.get(o.getItem().getProductId()).getTargetPrice())));
                       }
           else{
             threeCustomers.put(o.getCustomerId(),Math.abs((o.getItem().getSalesPrice() - product.get(o.getItem().getProductId()).getTargetPrice())));
           }
       }
       
       List<Map.Entry<Integer, Integer>> custList = new ArrayList<Map.Entry<Integer, Integer>>(threeCustomers.entrySet());
        
        Collections.sort(custList, new Comparator<Map.Entry<Integer, Integer>>(){
            @Override
            public int compare(Map.Entry<Integer, Integer> c1,
                               Map.Entry<Integer, Integer> c2)
            {
                return (c1.getValue()).compareTo(c2.getValue());
            }             
        });
        
        // Print top 3 customers
        System.out.println("\n Our Top 3 Customers:\n");
        for(int j = 0;j<3;j++){
            System.out.println(" Customer: " +custList.get(j)+" ");
        }
          
    }
     
      public void topThreeSalesPersons(){
           LinkedHashMap<Integer, Integer> top3salePerson = new LinkedHashMap<Integer, Integer>();
       Map<Integer, Order> order = DataStore.getInstance().getOrders();
       Map<Integer, Product> prod = DataStore.getInstance().getProducts();
      
       for(Order salesPerson:order.values())
       {
           if(top3salePerson.containsKey(salesPerson.getSupplierId())){
               int profit = top3salePerson.get(salesPerson.getSupplierId());
               int qty = salesPerson.getItem().getQuantity();
               top3salePerson.put(salesPerson.getSupplierId(),profit +((salesPerson.getItem().getSalesPrice() - prod.get(salesPerson.getItem().getProductId()).getTargetPrice()))*qty);
                       }
           else{               
                int qty = salesPerson.getItem().getQuantity();
               top3salePerson.put(salesPerson.getSupplierId(),((salesPerson.getItem().getSalesPrice() - prod.get(salesPerson.getItem().getProductId()).getTargetPrice()))*qty);
           }
       }
       
        List<Map.Entry<Integer, Integer>> entrylist = new ArrayList<Map.Entry<Integer, Integer>>(top3salePerson.entrySet());
        
        Collections.sort(entrylist, new Comparator<Map.Entry<Integer, Integer>>(){
            @Override
            public int compare(Map.Entry<Integer, Integer> s1,
                               Map.Entry<Integer, Integer> s2)
            {
                return (s2.getValue()).compareTo(s1.getValue());
            }     
             
        });
        
        // Print top 3 sales persons
        System.out.println("\n \n Top 3 Sales Persons :");
        for(int j = 0;j<3;j++){
            System.out.println("\n Sales Person: " +entrylist.get(j)+" ");
        }
       
       int sum = 0;
         for (Map.Entry<Integer,Integer> entry : top3salePerson.entrySet()) 
         {
              sum = sum + entry.getValue();
         }
         // Print the total revenue which comes from the profits from all the sales persons
         if(sum<0)
         {
              System.out.println("Total Revenue is = "  + sum); 
         }
         else
         {
             System.out.println("Total Revenue is = "  + sum);
         }
         }
        
        // Q5
     public void originalCatalogue() {
         
         
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, Integer> prod_target = new HashMap<>();
        Map<Integer, Integer> prod_quant = new HashMap<>();
        Map<Integer, Float> prod_salesprice = new HashMap<>();

        Map<Integer, Float> price_diff = new HashMap<>();
        for (Product p : products.values()) {
            prod_target.put(p.getProductId(), p.getTargetPrice());
            prod_quant.put(p.getProductId(), 0);
            prod_salesprice.put(p.getProductId(), 0.0f);
            price_diff.put(p.getProductId(), 0.0f);
            prod_avg.put(p.getProductId(), 0.0f);

        }
        System.out.println("-"
                + "-_____________________________________________-");
        for (Order o : orders.values()) {
            int quant = prod_quant.get(o.getItem().getProductId());
            float sales = prod_salesprice.get(o.getItem().getProductId());

            quant += o.getItem().getQuantity();
            sales += o.getItem().getQuantity() * o.getItem().getSalesPrice();
            prod_quant.put(o.getItem().getProductId(), quant);
            prod_salesprice.put(o.getItem().getProductId(), sales);
        }
        System.out.println("-______________________________________________-");
        for (int i : prod_salesprice.keySet()) {
            float avg = prod_salesprice.get(i) / prod_quant.get(i);

            prod_avg.put(i, avg);
            float diff = avg - prod_target.get(i);
            price_diff.put(i, diff);
        }

        System.out.println("Average Sales Price of each product: " + prod_avg);
        System.out.println("Target Price of each product: " + prod_target);
        System.out.println("Difference between the average sale price and target price of each product: " + price_diff);

        System.out.println("***Products whose Average Price lower than Target price****");
        System.out.println("-------------------------------------------------------------");
        System.out.println("Product_id" + "\t" + "Target_Price");
        System.out.println("-------------------------------------------------------------");
        for (int pid : prod_target.keySet()) {
            if (prod_avg.get(pid) < prod_target.get(pid)) {
                System.out.println(pid + "\t\t" + prod_target.get(pid));
            }
        }

        System.out.println("**Products whose Average Price greater than Target price**");
        System.out.println("-------------------------------------------------------------");
        System.out.println("Product_id" + "\t" + "Target_Price");
        System.out.println("-------------------------------------------------------------");
        for (int pid : prod_target.keySet()) {
            if (prod_avg.get(pid) >= prod_target.get(pid)) {
                System.out.println(pid + "\t\t" + prod_target.get(pid));
            }
        }

    }
     public void modifyCatalogue() {
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, Integer> prod_target = new HashMap<>();
        Map<Integer, Float> prod_profit = new HashMap<>();
        Map<Integer, Float> prod_diff = new HashMap<>();
        Map<Integer, Integer> modprices = new HashMap<>();
        Map<Integer, Float> prod_error = new HashMap<>();
        Map<Integer, Float> prod_mod_diff = new HashMap<>();

        for (Product p : products.values()) {
            prod_target.put(p.getProductId(), p.getTargetPrice());
        }
        for (Product p : products.values()) {
            prod_profit.put(p.getProductId(), 0.0f);
        }
        float p = 0;
        for (Order o : orders.values()) {
            p = (prod_avg.get(o.getItem().getProductId()) - prod_target.get(o.getItem().getProductId())) * 100 / prod_target.get(o.getItem().getProductId());
            prod_profit.put(o.getItem().getProductId(), p);

        }
        List<Integer> mapKeys = new ArrayList<>(prod_profit.keySet());
        List<Float> mapValues = new ArrayList<>(prod_profit.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);
        LinkedHashMap<Integer, Float> sortedMap = new LinkedHashMap<>();

        Iterator<Float> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Float val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                Float comp1 = prod_profit.get(key);
                Float comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }

        ArrayList<Integer> prodid = new ArrayList<Integer>(sortedMap.keySet());
        int top_avg_perct = Math.round(sortedMap.get(prodid.get(19)) + sortedMap.get(prodid.get(18)) + sortedMap.get(prodid.get(17))) / 3;


        for (int pid : prod_target.keySet()) {
            float mt = 0;
            float e = 0;
            int t = prod_target.get(pid);
            mt = (100 * prod_avg.get(pid)) / (100 + top_avg_perct);
            e = ((mt - t) / t) * 100;
            if (e > 0 && e > 5) {
                mt = (float) 1.05 * t;
            } else if (e < 0 && e < -5) {
                mt = (float) 0.95 * t;
            } else {
                mt = mt;
            }

            int mt1 = (int) mt;

            prod_error.put(pid, e);
            modprices.put(pid, mt1);
            Float diff = (prod_avg.get(pid) - prod_target.get(pid));
            Float mod_diff = (prod_avg.get(pid) - modprices.get(pid));
            prod_diff.put(pid, diff);
            prod_mod_diff.put(pid, mod_diff);
        }

        Collections.reverse(prodid);
        System.out.println("**Products whose Average Price lower than Modified Target price**");

        System.out.println("-------------------------------------------------------------");
        System.out.println("Product_id" + "\t\t" + "Modified_Target_Price");
        System.out.println("-------------------------------------------------------------");
        for (int pid : prod_target.keySet()) {
            if (prod_avg.get(pid) < modprices.get(pid) ) {
                System.out.println(pid + "\t\t" + modprices.get(pid));
            }
        }
        
        System.out.println("*Products whose Average Price greater than Modified Target price*");
        System.out.println("-------------------------------------------------------------");
        System.out.println("Product_id" + "\t" + "Modified_Target_Price");
        System.out.println("-------------------------------------------------------------");
        for (int pid : prod_target.keySet()) {
            if (prod_avg.get(pid) >= modprices.get(pid)) {
                System.out.println(pid + "\t\t" + modprices.get(pid));
            }
        }

        System.out.println("*For Original Data*");
        System.out.println("------------------------------------------------------------------------------------------------------------------");
        System.out.println("Productid       Average_sales_Price     Traget_Price   Difference_between_Average_price_Target_Price   ");
        System.out.println("------------------------------------------------------------------------------------------------------------------");
        for (int printid : prod_target.keySet()) {
            System.out.println(printid + "\t\t" + prod_avg.get(printid) + "\t\t" + prod_target.get(printid) + "\t\t" + prod_diff.get(printid));
        }

        System.out.println("*For Modified Target Data*");
        System.out.println("------------------------------------------------------------------------------------------------------------------");
        System.out.println("Productid       Average_sales_Price     Modified_Tragets        Difference_in_price     Error");
        System.out.println("------------------------------------------------------------------------------------------------------------------");
        for (int printid : prod_target.keySet()) {
            System.out.println(printid + "\t\t" + prod_avg.get(printid) + "\t\t\t" + modprices.get(printid) + "\t\t" + prod_mod_diff.get(printid) + "\t\t" + prod_error.get(printid));
        }

        //sort the table by the difference between the average sale price and target price of each product, 
        //from hight to low.
        List<Integer> proddiffmapKeys = new ArrayList<>(prod_diff.keySet());
        List<Float> proddiffmapValues = new ArrayList<>(prod_diff.values());
        Collections.sort(proddiffmapValues, Collections.reverseOrder());
        Collections.sort(proddiffmapKeys, Collections.reverseOrder());
        LinkedHashMap<Integer, Float> prodiffsortedMap = new LinkedHashMap<>();

        Iterator<Float> proddiffvalueIt = proddiffmapValues.iterator();
        while (proddiffvalueIt.hasNext()) {
            Float val = proddiffvalueIt.next();
            Iterator<Integer> proddiffkeyIt = proddiffmapKeys.iterator();

            while (proddiffkeyIt.hasNext()) {
                Integer key = proddiffkeyIt.next();
                Float comp1 = prod_diff.get(key);
                Float comp2 = val;

                if (comp1.equals(comp2)) {
                    proddiffkeyIt.remove();
                    prodiffsortedMap.put(key, val);
                    break;
                }
            }
        }
        System.out.println("Sorted original data based on difference between the average sale price and target price of each product, from hight to low." + prodiffsortedMap);

        //sort the table by the difference between the average sale price and target price of each product, 
        //from hight to low.
        List<Integer> prodmoddiffmapKeys = new ArrayList<>(prod_mod_diff.keySet());
        List<Float> prodmoddiffmapValues = new ArrayList<>(prod_mod_diff.values());
        Collections.sort(prodmoddiffmapValues, Collections.reverseOrder());
        Collections.sort(prodmoddiffmapValues, Collections.reverseOrder());
        LinkedHashMap<Integer, Float> prodmoddiffsortedMap = new LinkedHashMap<>();

        Iterator<Float> prodmoddiffvalueIt = prodmoddiffmapValues.iterator();
        while (prodmoddiffvalueIt.hasNext()) {
            Float val = prodmoddiffvalueIt.next();
            Iterator<Integer> prodmoddiffkeyIt = prodmoddiffmapKeys.iterator();

            while (prodmoddiffkeyIt.hasNext()) {
                Integer key = prodmoddiffkeyIt.next();
                Float comp1 = prod_mod_diff.get(key);
                Float comp2 = val;

                if (comp1.equals(comp2)) {
                    prodmoddiffkeyIt.remove();
                    prodmoddiffsortedMap.put(key, val);
                    break;
                }
            }
        }
        System.out.println("Sorted modified data based on difference between the average sale price and target price of each product, from hight to low. " + prodmoddiffsortedMap);

    }
}
