/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.ManageAirline;

import Business.TravelAgency;
import Business.Airline;
import Business.Airdirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import java.awt.Component;

/**
 *
 * @author Atulya
 */
public class ManageAirline extends javax.swing.JPanel {
    public Airdirectory airdirectory;
     private JPanel rightpanel;
     public TravelAgency travel;

    /**
     * Creates new form ManageAirline
     */
    
    public ManageAirline(JPanel rightpanel, TravelAgency travel) {
     initComponents();
        this.travel=travel;
        this.rightpanel=rightpanel;
       // this.airdirectory=airdirectory;
        populate();
    }
    public void populate(){
        DefaultTableModel dtm = (DefaultTableModel)tblairline.getModel();
        dtm.setRowCount(0);
        for (Airline airline: travel.getAirdirectory().getAirdirectory()){
            Object[] row = new Object[dtm.getColumnCount()];
            row[0]=airline;            
            row[1]= airline.getAirlineid();
           
            dtm.addRow(row);
        }
        }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblmanageairlines = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblairline = new javax.swing.JTable();
        btnupdate = new javax.swing.JButton();
        btncreate = new javax.swing.JButton();
        btndel = new javax.swing.JButton();
        btnback = new javax.swing.JButton();

        setBackground(new java.awt.Color(51, 51, 51));

        lblmanageairlines.setBackground(new java.awt.Color(0, 0, 0));
        lblmanageairlines.setFont(new java.awt.Font("Verdana", 1, 18)); // NOI18N
        lblmanageairlines.setForeground(new java.awt.Color(255, 255, 255));
        lblmanageairlines.setText("Manage Airlines");

        tblairline.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "AirlineID"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblairline);

        btnupdate.setBackground(new java.awt.Color(51, 51, 51));
        btnupdate.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnupdate.setText("View & Update Airline");
        btnupdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnupdateActionPerformed(evt);
            }
        });

        btncreate.setBackground(new java.awt.Color(51, 51, 51));
        btncreate.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btncreate.setText("Create Airline");
        btncreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncreateActionPerformed(evt);
            }
        });

        btndel.setBackground(new java.awt.Color(51, 51, 51));
        btndel.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btndel.setText("Delete an Airline");
        btndel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndelActionPerformed(evt);
            }
        });

        btnback.setBackground(new java.awt.Color(51, 51, 51));
        btnback.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnback.setText("Back");
        btnback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 802, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(280, 280, 280)
                            .addComponent(lblmanageairlines, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(105, 105, 105)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(110, 110, 110)
                            .addComponent(btncreate, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(151, 151, 151)
                            .addComponent(btnupdate))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(400, 400, 400)
                            .addComponent(btndel))
                        .addComponent(btnback))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 484, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lblmanageairlines)
                    .addGap(15, 15, 15)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(32, 32, 32)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btncreate)
                        .addComponent(btnupdate))
                    .addGap(13, 13, 13)
                    .addComponent(btndel)
                    .addGap(153, 153, 153)
                    .addComponent(btnback)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnupdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnupdateActionPerformed

        int selectedRow = tblairline.getSelectedRow();
        if(selectedRow >= 0)
        {
            Airline airline = (Airline)tblairline.getValueAt(selectedRow, 0);
            UpdateAirline update = new UpdateAirline(rightpanel, airline, travel);
            rightpanel.add("Updateairline", update);
            CardLayout layout = (CardLayout)rightpanel.getLayout();
            layout.next(rightpanel);
        }
        else{
            JOptionPane.showMessageDialog(null, "Please select a Row!!");
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_btnupdateActionPerformed

    private void btncreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncreateActionPerformed

        CreateAirline create = new CreateAirline(rightpanel, travel);
        rightpanel.add("Createairline", create);
        CardLayout layout = (CardLayout)rightpanel.getLayout();
        layout.next(rightpanel);
        // TODO add your handling code here:
    }//GEN-LAST:event_btncreateActionPerformed

    private void btndelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndelActionPerformed
        int selectedRow = tblairline.getSelectedRow();
        if(selectedRow>=0){
            int selectionButton = JOptionPane.YES_NO_OPTION;
            int selectionResult = JOptionPane.showConfirmDialog(null, "Are you sure to delete??","Warning",selectionButton);
            if(selectionResult == JOptionPane.YES_OPTION){
                Airline airline = (Airline)tblairline.getValueAt(selectedRow, 0);
                travel.getAirdirectory().deleteAirline(airline);
                populate();
            }
        }else{
            JOptionPane.showMessageDialog(null, "Please select a Row!!");
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_btndelActionPerformed

    private void btnbackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbackActionPerformed
        rightpanel.remove(this);
        CardLayout cardlayout = (CardLayout)rightpanel.getLayout();
        cardlayout.previous(rightpanel);

        // TODO add your handling code here:
    }//GEN-LAST:event_btnbackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnback;
    private javax.swing.JButton btncreate;
    private javax.swing.JButton btndel;
    private javax.swing.JButton btnupdate;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblmanageairlines;
    private javax.swing.JTable tblairline;
    // End of variables declaration//GEN-END:variables
}
