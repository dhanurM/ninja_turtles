/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author dhanur10
 */
public class Flightschedule {
    
    private ArrayList<Flight> flightschedule;
    
     public Flightschedule(){
        flightschedule = new ArrayList<Flight>();
      }
      public ArrayList<Flight> getFlightschdule() {
        return flightschedule;
    }
     
     public void setFlightschedule(ArrayList<Flight> flightschedule) {
        this.flightschedule = flightschedule;
    }
      
     public Flight addFlight() {
        Flight newflight = new Flight();
        flightschedule.add(newflight);
        return newflight;
    }
     
     public void deleteFlight(Flight flight){
        flightschedule.remove(flight);
    }
    
}
