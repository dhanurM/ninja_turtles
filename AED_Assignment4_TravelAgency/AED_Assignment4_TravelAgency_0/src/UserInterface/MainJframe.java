/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Flight;
import Business.Flightschedule;
import Business.TravelAgency;
import Business.Airdirectory;
import UserInterface.ManageAirline.ManageAirline;
import UserInterface.ManageBookings.Searchflight;
import UserInterface.manageflight.Manageflight;
import java.awt.CardLayout;
/**
 *
 * @author dhanur10
 */
public class MainJframe extends javax.swing.JFrame {
    
    private Airdirectory airdir;
    private Flightschedule flightschedule;
    private TravelAgency travel;
    private Flight flight;
 
    public MainJframe() {
        initComponents();
        airdir = new Airdirectory();
        travel = new TravelAgency();
        flightschedule = new Flightschedule();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Splitpane = new javax.swing.JSplitPane();
        leftpanel = new javax.swing.JPanel();
        ManageTA = new javax.swing.JButton();
        Manageflight = new javax.swing.JButton();
        Manageusers = new javax.swing.JButton();
        rightpanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Splitpane.setPreferredSize(new java.awt.Dimension(297, 27));

        leftpanel.setBackground(new java.awt.Color(51, 51, 51));

        ManageTA.setBackground(new java.awt.Color(255, 255, 255));
        ManageTA.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        ManageTA.setText("Manage Travel Agency");
        ManageTA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManageTAActionPerformed(evt);
            }
        });

        Manageflight.setBackground(new java.awt.Color(255, 255, 255));
        Manageflight.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        Manageflight.setText("Manage Flight Schedules");
        Manageflight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManageflightActionPerformed(evt);
            }
        });

        Manageusers.setBackground(new java.awt.Color(255, 255, 255));
        Manageusers.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        Manageusers.setText("Manage Customers");
        Manageusers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManageusersActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout leftpanelLayout = new javax.swing.GroupLayout(leftpanel);
        leftpanel.setLayout(leftpanelLayout);
        leftpanelLayout.setHorizontalGroup(
            leftpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, leftpanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(leftpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Manageusers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Manageflight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ManageTA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        leftpanelLayout.setVerticalGroup(
            leftpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(leftpanelLayout.createSequentialGroup()
                .addGap(102, 102, 102)
                .addComponent(ManageTA)
                .addGap(60, 60, 60)
                .addComponent(Manageflight)
                .addGap(60, 60, 60)
                .addComponent(Manageusers)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Splitpane.setLeftComponent(leftpanel);

        rightpanel.setBackground(new java.awt.Color(51, 51, 51));
        rightpanel.setLayout(new java.awt.CardLayout());
        Splitpane.setRightComponent(rightpanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Splitpane, javax.swing.GroupLayout.DEFAULT_SIZE, 826, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Splitpane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ManageTAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManageTAActionPerformed
     
        // TODO add your handling code here:
        ManageAirline airline = new ManageAirline(rightpanel, travel);
        rightpanel.add("Manageairline", airline);
        CardLayout layout = (CardLayout)rightpanel.getLayout();
        layout.next(rightpanel);
        
    }//GEN-LAST:event_ManageTAActionPerformed

    private void ManageflightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManageflightActionPerformed
    Manageflight mflight = new Manageflight(rightpanel,travel);
         rightpanel.add("Manageflight", mflight);
        CardLayout layout = (CardLayout)rightpanel.getLayout();
        layout.next(rightpanel);
        
// TODO add your handling code here:
    }//GEN-LAST:event_ManageflightActionPerformed

    private void ManageusersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManageusersActionPerformed
     Searchflight search = new Searchflight(rightpanel, travel);
     rightpanel.add("Searchflight", search);
     CardLayout layout = (CardLayout)rightpanel.getLayout();
     layout.next(rightpanel);
     //TODO add your handling code here:
    }//GEN-LAST:event_ManageusersActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJframe().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ManageTA;
    private javax.swing.JButton Manageflight;
    private javax.swing.JButton Manageusers;
    private javax.swing.JSplitPane Splitpane;
    private javax.swing.JPanel leftpanel;
    private javax.swing.JPanel rightpanel;
    // End of variables declaration//GEN-END:variables
}
