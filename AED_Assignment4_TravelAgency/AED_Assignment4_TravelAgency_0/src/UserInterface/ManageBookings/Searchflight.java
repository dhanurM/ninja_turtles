/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.ManageBookings;
import Business.Flight;
import Business.Flightschedule;
import Business.TravelAgency;
import Business.Airdirectory;
import UserInterface.manageflight.Updateflight;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import java.awt.Component;
import java.text.SimpleDateFormat;
/**
 *
 * @author tanishka
 */
public class Searchflight extends javax.swing.JPanel {

     private Flightschedule flightschedule;
    private JPanel rightpanel;
    private Airdirectory airdir;
    private TravelAgency travel;
    private Flight flight;
    SimpleDateFormat date = new SimpleDateFormat("dd-mm-yyyy");
    SimpleDateFormat time = new SimpleDateFormat("kk:mm");
    public Searchflight(JPanel rightpanel,  TravelAgency travel) {
        initComponents();
        this.travel=travel;
         this.rightpanel=rightpanel;
    }

   public void populatetbl(){
    DefaultTableModel dtm = (DefaultTableModel)tblflight.getModel();
        dtm.setRowCount(0);
        //SimpleDateFormat date = new SimpleDateFormat("dd-mm-yyyy");
        for (Flight flight: travel.getFlightschedule().getFlightschdule()){
            Object[] row = new Object[dtm.getColumnCount()];
            row[0]=flight;            
            row[1]= flight.getFlightcode();
            row[2]= flight.getSource();
            row[3]= flight.getDestination();
            row[4]= time.format(flight.getStarttime());
            row[5]= time.format(flight.getEndtime());
            row[6]= date.format(flight.getTraveldate());
            row[7]= flight.getPrice();
            row[8]= flight.getCapacity();
            dtm.addRow(row);
        }
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        lblsearch = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        btnsearch = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblflight = new javax.swing.JTable();
        btnbook = new javax.swing.JButton();

        setBackground(new java.awt.Color(51, 51, 51));

        jLabel2.setFont(new java.awt.Font("Verdana", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Search Flight Schedule");

        jComboBox1.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Departure", "Arrival", "Flightname" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        lblsearch.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        lblsearch.setForeground(new java.awt.Color(204, 204, 204));
        lblsearch.setText("Search by:");

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        btnsearch.setBackground(new java.awt.Color(51, 51, 51));
        btnsearch.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnsearch.setText("Search");
        btnsearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsearchActionPerformed(evt);
            }
        });

        tblflight.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        tblflight.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Flight code", "From", "To", "Departure", "Arrival", "Date", "Price", "Capacity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblflight);

        btnbook.setBackground(new java.awt.Color(51, 51, 51));
        btnbook.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnbook.setText("Book Flight");
        btnbook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbookActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 858, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(240, 240, 240)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(4, 4, 4)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblsearch, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(116, 116, 116)
                                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(160, 160, 160)
                            .addComponent(btnsearch))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 679, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnbook))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 682, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(61, 61, 61)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(1, 1, 1)
                            .addComponent(lblsearch))
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(16, 16, 16)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(btnsearch)
                    .addGap(123, 123, 123)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(29, 29, 29)
                    .addComponent(btnbook)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void btnsearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsearchActionPerformed
        DefaultTableModel dtm = (DefaultTableModel)tblflight.getModel();
        dtm.setRowCount(0);
        //SimpleDateFormat date = new SimpleDateFormat("dd-mm-yyyy");
        if(jComboBox1.getSelectedItem() == "Departure")
        {

            for (Flight flight: travel.getFlightschedule().getFlightschdule()){

                if(flight.getSource().toLowerCase().equals(jTextField1.getText().toLowerCase())){
                    Object[] row = new Object[dtm.getColumnCount()];
                    row[0]=flight;
                    row[1]= flight.getFlightcode();
                    row[2]= flight.getSource();
                    row[3]= flight.getDestination();
                    row[4]= time.format(flight.getStarttime());
                    row[5]= time.format(flight.getEndtime());
                    row[6]= date.format(flight.getTraveldate());
                    row[7]= flight.getPrice();
                    row[8]= flight.getCapacity();
                    dtm.addRow(row);
                    //populatetbl();
                }
            }
        }
        else if (jComboBox1.getSelectedItem() == "Arrival")
        {
            for (Flight flight: travel.getFlightschedule().getFlightschdule()){

                if(flight.getDestination().toLowerCase().equals(jTextField1.getText().toLowerCase())){
                    Object[] row = new Object[dtm.getColumnCount()];
                    row[0]=flight;
                    row[1]= flight.getFlightcode();
                    row[2]= flight.getSource();
                    row[3]= flight.getDestination();
                    row[4]= time.format(flight.getStarttime());
                    row[5]= time.format(flight.getEndtime());
                    row[6]= date.format(flight.getTraveldate());
                    row[7]= flight.getPrice();
                    row[8]= flight.getCapacity();
                    dtm.addRow(row);
                }
            }
        }
        else if (jComboBox1.getSelectedItem() == "Flightname")
        {
            for (Flight flight: travel.getFlightschedule().getFlightschdule()){

                if(flight.getFlightname().toLowerCase().equals(jTextField1.getText().toLowerCase())){
                    Object[] row = new Object[dtm.getColumnCount()];
                    row[0]=flight;
                    row[1]= flight.getFlightcode();
                    row[2]= flight.getSource();
                    row[3]= flight.getDestination();
                    row[4]= time.format(flight.getStarttime());
                    row[5]= time.format(flight.getEndtime());
                    row[6]= date.format(flight.getTraveldate());
                    row[7]= flight.getPrice();
                    row[8]= flight.getCapacity();
                    dtm.addRow(row);
                }
            }
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_btnsearchActionPerformed

    private void btnbookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbookActionPerformed

        int selectedRow = tblflight.getSelectedRow();
        if(selectedRow >= 0)
        {
            Flight flight = (Flight)tblflight.getValueAt(selectedRow, 0);
            Bookflight book = new Bookflight(rightpanel, flight, travel);
            rightpanel.add("Bookflight", book);
            CardLayout layout = (CardLayout)rightpanel.getLayout();
            layout.next(rightpanel);

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_btnbookActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnbook;
    private javax.swing.JButton btnsearch;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel lblsearch;
    private javax.swing.JTable tblflight;
    // End of variables declaration//GEN-END:variables
}
