/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Atulya
 */
public class Airline {
    private int id;
    private String Name;
    private int airlineid;
   
    public Airline(String Name, int airlineid) {
        this.Name = Name;
        this.airlineid = airlineid;
    }
   
    
    public String getAirlineName() {
        return Name;
    }

    public void setAirlineName(String Name) {
        this.Name = Name;
    }

    public int getAirlineid() {
        return airlineid;
    }

    public void setAirlineid(int airlineid) {
        this.airlineid = airlineid;
    }


    
     @Override
    public String toString() {
        return this.getAirlineName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
