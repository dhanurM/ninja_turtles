/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author tanishka
 */
public class Userdirectory {
    
    private ArrayList<User> userdirectory;
    
    public Userdirectory(){
        userdirectory = new ArrayList<User>();
      }
      public ArrayList<User> getUserdirectory() {
        return userdirectory;
    }
     
     public void setUserdirectory(ArrayList<User> userdirectory) {
        this.userdirectory = userdirectory;
    }
      
     public User addUser() {
        User newuser = new User();
        userdirectory.add(newuser);
        return newuser;
    }
     
     public void deleteUser(User user){
        userdirectory.remove(user);
    }
     
    
}
