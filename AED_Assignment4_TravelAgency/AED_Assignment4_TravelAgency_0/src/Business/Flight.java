/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author dhanur10
 */
public class Flight {
    private String flightname;
    private String flightcode;
    private String fromloc;
    private String toloc;
    private Date starttime;
    private Date endtime;
    private double price;
  //  private String flightdur;
    private Date traveldate;
    private int capacity;
    private ArrayList<String> seatcount;
    
    public Flight(){
        seatcount = new ArrayList<String>();                
    }

    public ArrayList<String> getSeatcount() {
        return seatcount;
    }

    public void setSeatcount(ArrayList<String> seatcount) {
        this.seatcount = seatcount;
    }
    public void addNoSeats(String temp){
        seatcount.add(temp);
    }

    public String getFlightname() {
        return flightname;
    }

    public void setFlightname(String flightname) {
        this.flightname = flightname;
    }

    public String getFlightcode() {
        return flightcode;
    }

    public void setFlightcode(String flightcode) {
        this.flightcode = flightcode;
    }

    public String getSource() {
        return fromloc;
    }

    public void setSource(String fromloc) {
        this.fromloc = fromloc;
    }

    public String getDestination() {
        return toloc;
    }

    public void setDestination(String toloc) {
        this.toloc = toloc;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getTraveldate() {
        return traveldate;
    }

    public void setTraveldate(Date traveldate) {
        this.traveldate = traveldate;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
      @Override
      public String toString() {
      return this.getFlightname(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
