/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dhanur10
 */
public class TravelAgency {
    
    public Airdirectory airdirectory;
    public Flightschedule flightschedule;
    public Userdirectory userdir;

   public TravelAgency ()
   {
   airdirectory = new Airdirectory();
   flightschedule = new Flightschedule();
   userdir= new Userdirectory();
   }
    public Airdirectory getAirdirectory() {
        return airdirectory;
    }

    public void setAirdirectory(Airdirectory airdirectory) {
        this.airdirectory = airdirectory;
    }

    public Flightschedule getFlightschedule() {
        return flightschedule;
    }

    public void setFlightschedule(Flightschedule flightschedule) {
        this.flightschedule = flightschedule;
    }

    public Userdirectory getUserdir() {
        return userdir;
    }

    public void setUserdir(Userdirectory userdir) {
        this.userdir = userdir;
    }
        
  
   
}