/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author Atulya
 */
public class Airdirectory {
 
     private ArrayList<Airline> airdirectory;
     
      //adding predefined arraylist
       public Airdirectory(){
        airdirectory = new ArrayList<Airline>();
        Airline a1 = new Airline("Qatar",1001);
        Airline a2 = new Airline("Emirates",1002);
        Airline a3 = new Airline("Lufthansa",1003);
        Airline a4 = new Airline("British Airways",1004);
        Airline a5 = new Airline("Etihad",1005);
        
        airdirectory.add(a1);
        airdirectory.add(a2);
        airdirectory.add(a3);
        airdirectory.add(a4);
        airdirectory.add(a5);
    }
      
     public ArrayList<Airline> getAirdirectory() {
        return airdirectory;
    }
     
     public void setAirdirectory(ArrayList<Airline> airdirectory) {
        this.airdirectory = airdirectory;
    }
      
      public Airline addAirline(String Name, int airlineid) {
        Airline newair = new Airline(Name,airlineid);
        airdirectory.add(newair);
        return newair;
    }
     
     public void deleteAirline(Airline airline){
        airdirectory.remove(airline);
        
    }
}
