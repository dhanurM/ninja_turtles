/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    // TODO
    
    public void userWithMostLikes(){
        Map<Integer,Integer> TotalLikesCount = new HashMap<>();
        Map<Integer,User> users = DataStore.getInstance().getUsers();
        
        for(User user : users.values()){
            for(Comment c : user.getComments()){
                int likes = 0;
                if(TotalLikesCount.containsKey(user.getId())){
                    likes = TotalLikesCount.get(user.getId());
                }
                likes += c.getLikes();
                TotalLikesCount.put(user.getId(), likes);
            }
        }
        int max=0;
        int maxId=0;
        for(int id : TotalLikesCount.keySet()){
            if(TotalLikesCount.get(id)>max){
                max = TotalLikesCount.get(id);
                maxId = id;
            }
        }
        System.out.println("\nUser with most like:" +max+ "\n" +users.get(maxId) +"\n");
    }
    
    
    // find 5 comments which have the most likes
    // TODO
    
    public void getFiveMostLikedComment(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        Collections.sort(commentList, new Comparator<Comment>(){
            
            @Override
            public int compare(Comment o1, Comment o2) {
                return o2.getLikes() -o1.getLikes();
            }
        });
        System.out.println("5 most liked comments :");
        for(int i =0; i < commentList.size() && i<5; i++) {
            System.out.println(commentList.get(i));
        }
        System.out.println("\n");
    }
    
    // Find the average number of likes per comment
     public void getAvgLikesperComment(){
         Map<Integer, Integer> TotalLikesCount= new HashMap<>();
        Map<Integer, User> users= DataStore.getInstance().getUsers();
        for(User user: users.values()){
            for(Comment c : user.getComments()){
                int likes=0;
                if(TotalLikesCount.containsKey(user.getId())){
                    likes = TotalLikesCount.get(user.getId());
                    
                }
                likes+= c.getLikes();
                TotalLikesCount.put(user.getId(), likes);
                
            }
            
            
        }
      int totallikes= (int) 1f;
      for (float f : TotalLikesCount.values()) {
      totallikes += f; 
      }
       System.out.println("1) Total number of likes="+totallikes);
       
       Map<Integer, Integer> TotalCommentsCount= new HashMap<>();
        Map<Integer, User> users2= DataStore.getInstance().getUsers();
        for(User user: users2.values()){
            for(Comment c : user.getComments()){
                int comments=0;
                if(TotalCommentsCount.containsKey(user.getId())){
                    comments = TotalCommentsCount.get(user.getId());
                    
                }
                comments++;
                TotalCommentsCount.put(user.getId(), comments);
                
            }     
        }
      float totalcomments= 1f;
      for (float f : TotalCommentsCount.values()) {
      totalcomments += f; 
      }
       System.out.println("Total number of comments="+totalcomments);
       float average;
       average= totallikes/totalcomments;
       System.out.println("Average likes per comment:"+ average);
    }
     
     //Top five posts with most liked comments
     
       public void getPostWithMostLikedComments(){
        Map<Integer, Comment> comments= DataStore.getInstance().getComments();
        List<Comment> commentList= new ArrayList<>(comments.values());
        
        Collections.sort(commentList, new Comparator<Comment>(){
            @Override
            public int compare(Comment o1, Comment o2){
                return o2.getLikes() - o1.getLikes();
            }   
            });
        System.out.println("\n"+ "2) Post with most liked comments:");
        for(int i=0; i< commentList.size() && i< 1; i++){
            System.out.println(commentList.get(i));
        }
    }
         public void postWithMostComments() {

        Map<Integer, Integer> noOfCommentsCount = new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        for (Post post : posts.values()) {
            int CommentsCount = 0;
            for (Comment comment : post.getComments()) {
                if (noOfCommentsCount.containsKey(post.getPostId())) {
                    CommentsCount++;
                } else {
                    CommentsCount++;
                }
                noOfCommentsCount.put(post.getPostId(), CommentsCount);
            }
        }

        int max = 0;
        int maxId = 0;
        for (int id : noOfCommentsCount.keySet()) {
            if (noOfCommentsCount.get(id) > max) {
                max = noOfCommentsCount.get(id);
                maxId = id;
            }

        }
        System.out.println("\n\n3).Post with most comments \nPost Id with most comments: " + maxId+ "\nTotal number of comments in that post: " + max);
       
    }
         //Top Five Inactive User based on Number of Posts
         
      public void FiveinactiveUserByPosts() {
        Map<Integer, Integer> totalPosts = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        int totalPostsbByUser = 0;
        for (Post post : posts.values()) {
            if (users.containsKey(post.getUserId())) {
                totalPosts.put(post.getUserId(), totalPosts.getOrDefault(post.getUserId(), 0) + 1);
            }
        }

        Map<Integer, Integer> totalPosts1 = sortByValue(totalPosts, 0);

        int i = 0;
        System.out.println("\n\n4) Top 5 inactive users based on total posts number.");
        System.out.println("\nTop 5 Inactive users by number of posts");
        for (Map.Entry<Integer, Integer> entry : totalPosts1.entrySet()) {
            if (i > 4) {
                break;
            } else {
                System.out.println("User Id: " + entry.getKey() + " No. of Posts: " + entry.getValue());
                i++;
            }
        }
    }


    private Map<Integer, Integer> sortByValue(Map<Integer, Integer> totalPosts, final int cond) {


        List<Map.Entry<Integer, Integer>> list
                = new LinkedList<Map.Entry<Integer, Integer>>(totalPosts.entrySet());


        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                if(cond == 0){
                return (o1.getValue().compareTo(o2.getValue()));
                }
                else{
                return (o2.getValue().compareTo(o1.getValue()));
                }
                
            }
        });

        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> m : list) {
            temp.put(m.getKey(), m.getValue());
        }
        return temp;
    }
    
    //Top Five Inactive Users Based on Comments
         
       public void FiveInactiveUsersComments(){
        Map<Integer,User> users= DataStore.getInstance().getUsers();
        Map<Integer,Comment> post= DataStore.getInstance().getComments();
        Map<Integer, Integer> userBasedOnComment =new HashMap();
        for (User user: users.values()){
            int commentCount =0;
            commentCount = user.getComments().size();
            userBasedOnComment.put(user.getId(), commentCount);
        }
        List<Map.Entry<Integer, Integer>> list=
                new LinkedList<>(userBasedOnComment.entrySet());
        
        
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>(){
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2){
            return o1.getValue() - o2.getValue();
        }
    });
        System.out.println("\n"+"5) Top 5 inactive users based on total comments they created: ");
        for(int i =0; i< list.size() && i<5; i++){
            System.out.println(users.get(list.get(i).getKey()));
        }
    }
       
       //Top Five Inactive Users Overall
       
       
       public void FiveInactiveUsersOverall(){ 
       
      Map<Integer , Integer> allInactive = new HashMap();
      Map<Integer,Post> post1 = DataStore.getInstance().getPosts(); 
      Map<Integer,User> user1 = DataStore.getInstance().getUsers();      
       for(User users : user1.values())
    {
        allInactive.put(users.getId(), users.getComments().size());
    }
 
       int a = 0;
       int b = 0;
       for(Post posts : post1.values())
    {
              if(allInactive.containsKey(posts.getUserId()))
        {
           b = posts.getUserId();
           a = allInactive.get(posts.getUserId());
           
           allInactive.put(b, ++a);
            
        }  
              
        else
           allInactive.put(posts.getUserId(),1);
    }

       
      Set<Map.Entry<Integer, Integer>> set = allInactive.entrySet();
        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(set);
        Collections.sort( list, new Comparator<Map.Entry<Integer, Integer>>()
        {
            public int compare( Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2 )
            {
                return o1.getValue() - o2.getValue();
            }
        } );
        
      System.out.println("\n"+"6) Top Five Inactive Users Overall:");
        for(int i =0; i< list.size() && i<5; i++){
           System.out.println(user1.get(list.get(i).getKey()) +"Overall Rating :" + list.get(i).getValue());
        }
   
    }
       
       //Top Five Pro-Active Users Overall
       
       public void FiveProActiveUsersOverall(){ 
       
      Map<Integer , Integer> allActive = new HashMap();
      Map<Integer,Post> post1 = DataStore.getInstance().getPosts(); 
      Map<Integer,User> user1 = DataStore.getInstance().getUsers();
     
      
       for(User users : user1.values())
    {
        allActive.put(users.getId(), users.getComments().size());
    }
       
        
       int d = 0;
       int c = 0;
       for(Post posts : post1.values())
    {
              if(allActive.containsKey(posts.getUserId()))
        {
           d = posts.getUserId();
           c = allActive.get(posts.getUserId());
           
           allActive.put(d, ++c);
            
        }  
              
        else
                  allActive.put(posts.getUserId(),1);
    }
       
      Set<Map.Entry<Integer, Integer>> set = allActive.entrySet();
        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(set);
        Collections.sort( list, new Comparator<Map.Entry<Integer, Integer>>()
        {
            public int compare( Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2 )
            {
                return o2.getValue() - o1.getValue();
            }
        } );

      System.out.println("\n"+"7) Top Five Pro-Active Users Overall:");
    for(int i =0; i< list.size() && i<5; i++){
           System.out.println(user1.get(list.get(i).getKey()) +"Overall Rating :" + list.get(i).getValue());
       } 
   }
}
